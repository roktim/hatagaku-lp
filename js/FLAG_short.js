(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {};
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {
	for(var i = 0; i < cacheList.length; i++) {
		if(cacheList[i].cacheCanvas)
			cacheList[i].updateCache();
	}
};

lib.addElementsToCache = function (textInst, cacheList) {
	var cur = textInst;
	while(cur != null && cur != exportRoot) {
		if(cacheList.indexOf(cur) != -1)
			break;
		cur = cur.parent;
	}
	if(cur != exportRoot) {
		var cur2 = textInst;
		var index = cacheList.indexOf(cur);
		while(cur2 != null && cur2 != cur) {
			cacheList.splice(index, 0, cur2);
			cur2 = cur2.parent;
			index++;
		}
	}
	else {
		cur = textInst;
		while(cur != null && cur != exportRoot) {
			cacheList.push(cur);
			cur = cur.parent;
		}
	}
};

lib.gfontAvailable = function(family, totalGoogleCount) {
	lib.properties.webfonts[family] = true;
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
	for(var f = 0; f < txtInst.length; ++f)
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);

	loadedGoogleCount++;
	if(loadedGoogleCount == totalGoogleCount) {
		lib.updateListCache(gFontsUpdateCacheList);
	}
};

lib.tfontAvailable = function(family, totalTypekitCount) {
	lib.properties.webfonts[family] = true;
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
	for(var f = 0; f < txtInst.length; ++f)
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);

	loadedTypekitCount++;
	if(loadedTypekitCount == totalTypekitCount) {
		lib.updateListCache(tFontsUpdateCacheList);
	}
};
// symbols:



// stage content:
(lib.FLAG_short = function(mode,startPosition) {
	this.initialize(mode,startPosition,{});

	// ハタガク文字要素
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKCUIAAhyIgoAAIA6i0IArAAIgUA+IAADog");
	this.shape.setTransform(73.5,275.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AADhQIgNAAIAADkIgkAAIAAkmIAkAAIAAAcIA4AAIAADgQAAAQgNAMQgOAMgQACg");
	this.shape_1.setTransform(95.1,275.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag2CUIAAgfIAoAAIAAgQIghAAIAAgeIAhAAIAAgQIgoAAIAAhvIAoAAIAAgQIgiAAIAAgdIAiAAIAAgQIgUAAIAAgdIBYAAIAAAdIgnAAIAAAQIAnAAIAAAdIgnAAIAAAQIAnAAIAABvIgnAAIAAAQIAiAAIAAAeIgiAAIAAAQIAnAAIAAAfgAAPAcIAQAAIAAgQIgQAAgAgeAcIAQAAIAAgQIgQAAgAAPgOIAQAAIAAgQIgQAAgAgegOIAQAAIAAgQIgQAAg");
	this.shape_2.setTransform(83.5,275.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAWCPQhRhggJgNQgQgWAAgTQABgXAPgSIBVheIBCAAIhABHIgsAzQgEAFAAAGQAAAHAFAFIBsCMg");
	this.shape_3.setTransform(118.1,276);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AglCFQgPgLgFgVQgFgTAIgTQAKgWAXgUIAZgTQAAgJgEgEQgEgFgLADQgIADgJAHQgNAJgRAVQgbAdgVARIAAg+QAQgMASgZQAKgNALgUIAKgSIg6AAIAAgsIBJAAIAHgaIArAAIgHAaIBRAAIAAAsIheAAIgXApQAIgHAKgGQASgLANAIQANAJAEAZIASgJQAYgJAagHIAAAvQgUAEgZALIgVAKIAAAuIgnAAIAAgcIgMAMQgMAPADALQAEAQAVAAIAlAAQAnAAAOgBIAAAvIhMAAIgIABQghAAgUgPg");
	this.shape_4.setTransform(147,275.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgOCUIgNgtIBOAAQARAAABgQIAAgLIiDAAIAAgnIg+ArIAAgwIBEgvIhSAAIAAgsIBvAAIAAgfIhTAAIAAgqIBTAAIAAgOIApAAIAAAOIBGAAIAAAVIAhgYIAWAfIhAAuIAAAgIAggXIAAAvIgxAiIA3AAIAAA6QgCAagRAQQgRAPgaABgAgSAGIAAAaIALAAIBBgvIguAAgAAOg/IAlgbIglAAg");
	this.shape_5.setTransform(181.8,275.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ag8AWIAAgsIB4AAIAAAsg");
	this.shape_6.setTransform(214.4,263.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AA5BtQgZABgMgLQgLgKAAgbIAAgRQAAgUgIgEIgCgBQgGABgNAKIhSBOIAAhGIBshjIhoAAIAAgwIC2AAIAAAtIg5A1IAJABQAKABAEAIQADAFAAAPIABAcQABALAKACIAmAAIAAAwg");
	this.shape_7.setTransform(214.8,279.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhVBfQgCgTANgQQAQgUAjABQAmAAANAVQAHALgCALQAGgDAEgGQALgMgCgRQgDgdgrgFQgvgHg6AgIAAgzIBchRIhPAAIAAgwICYAAIAAAtIhOBFIAdgCQAjABAUARQAcAWAAAoQAAAwgjAaQgbAUghABIgDAAIgFAAQhMAAgGgxgAglBOQgGADAAAGQAAAEADADQAFAFAJABQAJABAFgEQAFgDABgFQABgEgEgDQgFgGgKgBIgCAAQgHAAgEADg");
	this.shape_8.setTransform(245.2,275.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAGAmIgDgCIgCACQgdAXgwAMIgMguIADgCQAjgIATgOIADgDIgigrIAogbIAaAiIAagkIApAbIghArIgCADIACACQAWASAdAIIgMAsQgogJgfgag");
	this.shape_9.setTransform(317.4,283.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhiCWIAAiGIgLAWIgfgiIAphRIgfAAIAAguIAgAAIAAgaIAvAAIAAAaIBMAAIAAgaIAvAAIAAAaIBFAAIAAAuIgSAAIAGAbQAEAdAAAHIgxAMQgBgWgDgPIgGgZIgDgNIgXAAIgHAlIgNAoIgvgLIALgiIAHggIgyAAIAAAZIAVAAIALBiIggAAIAABog");
	this.shape_10.setTransform(312.4,275.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgZARIAFghIAuAAIgFAhg");
	this.shape_11.setTransform(284.5,261.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgXARIAAghIAuAAIAAAhg");
	this.shape_12.setTransform(277,261.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgXARIAAghIAuAAIAAAhg");
	this.shape_13.setTransform(269.4,261.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AhLAzIAxAAQAIAAAEgGQAEgGgBgHIgFgRIhlAAIAAgrIBjAAIAIgXIg7AAIAAguIB6AAIgWBFIBWAAIAAArIhSAAIAFAXQAFAYgNASQgMAOgYAEIhHAAg");
	this.shape_14.setTransform(277.7,280.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("ABSAiIAAgbIilAAIAAAbIguAAIAAhDIEEAAIAABDg");
	this.shape_15.setTransform(277,268.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AhYFoIBIrPIBpAAIhJLPg");
	this.shape_16.setTransform(151.9,135.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhzFoIB9rPIBqAAIh9LPg");
	this.shape_17.setTransform(123.9,135.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("Ag7FoIC3plIifAAIh3F2IhuAAICXngIF6AAIjXLPg");
	this.shape_18.setTransform(306.8,135.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("Ag0FoIBKj+IilAAIgTA/IhuAAICnoQIF6AAIjYLPgAhuAAICkAAIBNj9IifAAg");
	this.shape_19.setTransform(193.5,135.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("Ag+A5IAUhxIBpAAIgUBxg");
	this.shape_20.setTransform(276.1,99.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("Ag+A5IAUhxIBpAAIgUBxg");
	this.shape_21.setTransform(262,99.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAcFoIBaoDIiLAAIhIGdIgoBmIhsAAIAuh3IBGmMIhtAAIAShqIBtAAIARhiIBpAAIgRBiID0AAIhtJtg");
	this.shape_22.setTransform(248.5,135.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},99).wait(1));

	// 6
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("Aj1TOMAEwgmbIC7AAMgEwAmbg");
	this.shape_23.setTransform(43.4,167.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(11).p("A3dsNIA8AKQBMAKBSAGQEHARDggtQBVgRBWgeQAygSBaglQCkg/CfgEQBvgCBwAkQBCAVCHA+QCHA+BTAaQCGArCOAIQELAPFXhAQCsggB2gkQg2BjhGCUQiMEohQD4QhqFEgIFJQgFClARBjQg+AShjAPQjHAgi/gJQhugGh4gpQg7gUiYhDQiHg9hZgaQiIgpiMgEQiRgFiPAoQinA4hfAfQiuA6ipAWQjoAfkrgUg");
	this.shape_24.setTransform(235.5,136.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_24},{t:this.shape_23}]},79).to({state:[{t:this.shape_24},{t:this.shape_23}]},20).wait(1));

	// 4
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Aj1TOMAEwgmbIC7AAMgEwAmbg");
	this.shape_25.setTransform(43.4,167.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(11).p("A2TtDIDPgxQDrgyCJgEQESgHDJA7QA3ARBzAjQBtAeB3APQEWAiD/gXQC/gRDtg5QCrgpD1gbQB6gNBYgFQgnBagyCVQhjEogzEiQg9FdACFZQABCsANBmQhVgNh6gGQj1gNi8AgQibAdhSAOQiQAahrAKQkrAekGg7Qg5gNi2g4QidgwhvgTQlcg5mgCFg");
	this.shape_26.setTransform(228.1,141.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_26},{t:this.shape_25}]},77).to({state:[{t:this.shape_26},{t:this.shape_25}]},1).to({state:[]},1).wait(21));

	// 3
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("Aj1TOMAEwgmbIC7AAMgEwAmbg");
	this.shape_27.setTransform(43.4,167.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(11).p("A2isZIDfANQEMAQDjAVQFBAfGfhVQCKgcCQgqQBRgYCLgsQDmhHCnAGQDnAJENBUQCGAqBYApQgbAwgrBoQhXDQhSEUQhhFHgtGBQgWDAgCB/QgrgXhKgWQiTgsiYAEQjYAFikAvQjBA/idAmQjDAuhTAPQiXAaiiADQl1AGr+h4g");
	this.shape_28.setTransform(229.6,137.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_28},{t:this.shape_27}]},74).to({state:[{t:this.shape_28},{t:this.shape_27}]},2).to({state:[]},1).wait(23));

	// 2
	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("Aj1TOMAEwgmbIC7AAMgEwAmbg");
	this.shape_29.setTransform(43.4,167.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#FFFFFF").ss(11).p("A2lsfIA7APQBLARBSAMQEFApDkgTQCJgLDUgrQBKgOExhEQHUhoCaARQC8AWCcBTQBYAvAcALQBEAdBQAKQCTATCaACQBNABAwgDQgRBLghB8QhBD5hRD3QhpFFgkFxQgSC5ADB4QgnAChCgIQiDgRiCg2Qg9gZhNgqQhWgughgPQiPhBjkgQQi1gOjLApQjzA9iLAiQkBA/jmAWQk+Afl6geg");
	this.shape_30.setTransform(229.9,137.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_30},{t:this.shape_29}]},71).to({state:[{t:this.shape_30},{t:this.shape_29}]},2).to({state:[]},1).wait(26));

	// 1
	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("Aj1TOMAEwgmbIC7AAMgEwAmbg");
	this.shape_31.setTransform(43.4,167.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#FFFFFF").ss(11).p("A3dsNIA8AKQBMAKBSAGQEHARDggtQBVgRBWgeQAygSBaglQCkg/CfgEQBvgCBwAkQBCAVCHA+QCHA+BTAaQCGArCOAIQELAPFXhAQCsggB2gkQg2BjhGCUQiMEohQD4QhqFEgIFJQgFClARBjQg+AShjAPQjHAgi/gJQhugGh4gpQg7gUiYhDQiHg9hZgaQiIgpiMgEQiRgFiPAoQinA4hfAfQiuA6ipAWQjoAfkrgUg");
	this.shape_32.setTransform(235.5,136.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32},{t:this.shape_31}]}).to({state:[]},71).wait(29));

	// 背景
	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("rgba(255, 255, 255, 0)").s().p("EghlAZyMAAAgzjMBDLAAAMAAAAzjg");
	this.shape_33.setTransform(215,165);

	this.timeline.addTween(cjs.Tween.get(this.shape_33).wait(10000000000));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(215,165,430,330);
// library properties:
lib.properties = {
	id: '11DEF3584FC44446BA38086E821AA7D7',
	width: 420,
	height: 330,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	// an.bootstrapListeners.push(fnCallback);
	// if(an.bootcompsLoaded.length > 0) {
	// 	for(var i=0; i<an.bootcompsLoaded.length; ++i) {
	// 		fnCallback(an.bootcompsLoaded[i]);
	// 	}
	// }
};

an.compositions = an.compositions || {};
an.compositions['11DEF3584FC44446BA38086E821AA7D7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;
