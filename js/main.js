/* ==========================================================================
Author:RoktimSazib
url:http://www.codingavatar.com
description:Hatagaku-LP project
========================================================================== */

$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 500, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
  });

  $(window).on('load resize', function(){
    var screenW = $(window).width();
    var flagLeft = (screenW - 920) / 2;
    if(window.outerWidth >= 1000) {
       $(".left_banner").css('width', 'calc(100% - 260px)');
       $(".left_part").css('width', 'calc(100% - 282px)');
       $(".text_area").css('left', flagLeft);
    } else {
      $(".left_banner").css('width', 'calc(1000px - 260px)');
      $(".left_part").css('width', 'calc(1000px - 282px)');
      // $(".text_area").css('left', flagLeft);
    }
  });

  $(window).scroll(function(){
    if ($(this).scrollTop() > 200) {
      $('.scroll_top').fadeIn();
    } else {
      $('.scroll_top').fadeOut();
    }
  });

  //Click event to scroll to top
  $('.scroll_top').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });
setTimeout(function(){
    $("#b_btn").animate({ width:'100%' }, {duration:800});
}, 1500);
/*preload indicator*/
$(window).load(function() {
    $("#status").fadeOut();
    $("#preloader").delay(1000).fadeOut("slow");
});
